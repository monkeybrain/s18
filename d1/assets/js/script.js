let myGrades = [98, 95, 75, 99, 100];

Object
	- is a collection of related data and/or functionality
		- which usually consists of several variables and functions
			- which are called properties and methods when they are inside objects.

cellphone - an object
properties(variables)
 - color				
 - weight
 - unit/model
 - brand
functions(methods)
	- alarm
	- open
	- close
	- ringing
	- send messages

Creating an object in JavaScript
	1. let cellphone = Object();
	2. let cellphone = {};

Accessing properties and methods of an object
	- To access the properties and methods we will use the dot notation

syntax:
	object.properties
	object.methods()

Example:
	cellphone.color
	cellphone.ring()

Re-assigning/Editing
	- to re-assign values of properties and methods just use the assignment operator (=)

	Ex. 
		cellphone.color = "blue";

Delete properties or method of an object
	- to delete use the keyword "delete"

	Ex. 
		delete cellphone.color
		delete cellphone.ring

Object constructor
 - solve our problem in creating multiple object
 - is a function that initialiazes an object

// create object
let cellphone = {
	color: "black",
	weight: "115 grams",
	model: "iPhone10",
	brand: "iPhone",
	ring: function(){
		console.log("Cellphone is ringing.")
	}
};

console.log(cellphone);

let car = Object();
car.color = "red";
car.model = "Vios 2021";
car.drive = function(){
	console.log("car is running.")
};

console.log(car);

cellphone.color = "blue";
console.log(cellphone.color);
cellphone.ring = function(){
	console.log('The blue cellphone is ringing.');
};
console.log(cellphone.ring());

// to delete properties and objects
delete cellphone.color;
delete cellphone.ring;
console.log(cellphone);

/*Object constructor
Case:
	Imagine we are going to create a pokemon with multiple properties
		- name
		- element
		- level
		- health
		- attack
		- action

let pokemon = {
	name : "Pikachu",
	element : "electric",
	level : 3,
	health : 100,
	attack : 50,
	action : function(){
		console.log("This pokemon attact targetPokemon");
		console.log("targetPokemon's health is now reduced.")
	}
};
console.log(pokemon); */

// object constructor
// function Pokemon(name, level, element){
// 	this.name = name;
// 	this.level = level;
// 	this.element = element;
// 	this.health = 100 + (2 * level);
// 	this.attack = level * 1.25;
// 	this.action = function(){
// 		console.log(this.name + "tackled targetPokemon");
// 		console.log("targetPokemon's health is now reduced.")
// 	};
// 	this.die = function(){
// 		console.log(this.name + "died.")
// 	}
// };

// let pickachu = new Pokemon("Pickachu", 16, "electric");
// let ratata = new Pokemon("Ratata", 8, "ground");
// console.log(pickachu);
// console.log(ratata);

function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 10;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
};

let Pickachu = new Pokemon("Pickachu");
let Geodude = new Pokemon("Geodude");

Pickachu.attack(Geodude);

