console.log("Hello World");

function Trainer(name, age, city){
	this.name = name;
	this.age = age;
	this.city = city;
};

let Trainer1 = new Trainer("Ash", 21, "Palett Town");

console.log(Trainer1);

function Pokemon(name){
	this.name = name;
	this.level = 12;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
}
}
let bulbasaur = new Pokemon("Bulbasaur");
let charmander = new Pokemon("Charmander");

bulbasaur.attack(charmander);